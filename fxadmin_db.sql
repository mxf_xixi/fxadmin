/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : fxadmin_db

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 17/03/2021 16:05:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fx_album_class
-- ----------------------------
DROP TABLE IF EXISTS `fx_album_class`;
CREATE TABLE `fx_album_class`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '相册id',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '相册名称',
  `class_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相册封面',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_default` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为默认相册,1代表默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '相册分类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_album_class
-- ----------------------------
INSERT INTO `fx_album_class` VALUES (1, '默认相册', 'http://fxadmin.com/data/upload/20210302//20210302153609_859.png', '2021-03-15 14:37:10', 0);
INSERT INTO `fx_album_class` VALUES (2, '分类', 'http://fxadmin.com/data/upload/20210315//20210315143558_215.png', '2021-03-15 14:37:15', 0);

-- ----------------------------
-- Table structure for fx_album_pic
-- ----------------------------
DROP TABLE IF EXISTS `fx_album_pic`;
CREATE TABLE `fx_album_pic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NULL DEFAULT 0 COMMENT '分类id',
  `image_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `image_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '相册图片' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for fx_auth_api
-- ----------------------------
DROP TABLE IF EXISTS `fx_auth_api`;
CREATE TABLE `fx_auth_api`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `rule_id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接口名称',
  `api_url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '接口路由',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20029 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '接口路由表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_auth_api
-- ----------------------------
INSERT INTO `fx_auth_api` VALUES (20000, 33, '菜单列表-api', 'v1/auth/auth-rule/auth-rule-list', NULL);
INSERT INTO `fx_auth_api` VALUES (20001, 33, '菜单编辑-api', 'v1/auth/auth-rule/edit-auth-rule', NULL);
INSERT INTO `fx_auth_api` VALUES (20002, 33, '菜单删除-api', 'v1/auth/auth-rule/del-auth-rule', NULL);
INSERT INTO `fx_auth_api` VALUES (20003, 37, '角色列表-api', 'v1/auth/role/role-list', NULL);
INSERT INTO `fx_auth_api` VALUES (20004, 37, '角色编辑-api', 'v1/auth/role/role-edit', NULL);
INSERT INTO `fx_auth_api` VALUES (20005, 37, '角色删除-api', 'v1/auth/role/role-del', NULL);
INSERT INTO `fx_auth_api` VALUES (20006, 37, '角色权限查看-api', 'v1/auth/role/role-auth-list', NULL);
INSERT INTO `fx_auth_api` VALUES (20007, 37, '角色权限编辑-api', 'v1/auth/role/role-auth-edit', NULL);
INSERT INTO `fx_auth_api` VALUES (20008, 38, '列表-api', 'v1/auth/user/list', NULL);
INSERT INTO `fx_auth_api` VALUES (20009, 38, '编辑-api', 'v1/auth/user/user-edit', NULL);
INSERT INTO `fx_auth_api` VALUES (20010, 38, '删除-api', 'v1/auth/user/user-del', NULL);
INSERT INTO `fx_auth_api` VALUES (20012, 41, '获取信息', 'v1/test/info', '2021-02-02 22:10:43');
INSERT INTO `fx_auth_api` VALUES (20016, 40, '分类列表-api', 'v1/album-class/list', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20017, 40, '分类编辑-api', 'v1/album-class/edit', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20018, 40, '分类删除-api', 'v1/album-class/del', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20019, 40, '分类信息-api', 'v1/album-class/info', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20020, 40, '图片列表-api', 'v1/album-pic/list', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20021, 40, '图片编辑-api', 'v1/album-pic/edit', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20022, 40, '图片删除-api', 'v1/album-pic/del', '2021-03-01 18:12:39');
INSERT INTO `fx_auth_api` VALUES (20023, 45, '列表-api', 'v1/test/list', '2021-03-02 16:04:52');
INSERT INTO `fx_auth_api` VALUES (20025, 45, '删除-api', 'v1/test/del', '2021-03-02 16:04:52');
INSERT INTO `fx_auth_api` VALUES (20027, 47, '测试修改-api', 'v1/test/edit', '2021-03-15 15:27:23');
INSERT INTO `fx_auth_api` VALUES (20028, 47, '测试信息-api', 'v1/test/info', '2021-03-15 15:28:52');

-- ----------------------------
-- Table structure for fx_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `fx_auth_rule`;
CREATE TABLE `fx_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT NULL COMMENT '父id',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `sort` int(11) NULL DEFAULT 1 COMMENT '排序 ',
  `type` tinyint(4) NULL DEFAULT 1 COMMENT '1平台路由',
  `is_menu` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否为菜单 1是',
  `desc` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_auth_rule
-- ----------------------------
INSERT INTO `fx_auth_rule` VALUES (8, 0, '菜单权限管理', '', 1, 1, '1', '菜单管理', 'iconfont icon-fenlei', NULL);
INSERT INTO `fx_auth_rule` VALUES (33, 8, '菜单列表', '/page/auth/menu_list', 1, 1, '1', '菜单列表', 'iconfont icon-gouxuan1', NULL);
INSERT INTO `fx_auth_rule` VALUES (37, 8, '角色列表', '/page/auth/role_list', 2, 1, '1', '用户角色', 'iconfont icon-gouxuan1', NULL);
INSERT INTO `fx_auth_rule` VALUES (38, 8, '用户列表', '/page/auth/user_list', 3, 1, '1', '用户列表', 'iconfont icon-gouxuan1', NULL);
INSERT INTO `fx_auth_rule` VALUES (39, 0, '平台管理', NULL, 4, 1, '1', '平台管理', 'iconfont icon-fenlei', '2021-03-17 14:32:54');
INSERT INTO `fx_auth_rule` VALUES (40, 39, '相册管理', '/page/common/album/album', 3, 1, '1', '相册管理', 'iconfont icon-fenlei', '2021-02-02 21:34:03');
INSERT INTO `fx_auth_rule` VALUES (42, 0, '测试管理', '', 4, 1, '1', '测试管理', 'iconfont icon-fenlei', '2021-03-02 16:06:39');
INSERT INTO `fx_auth_rule` VALUES (45, 42, '测试列表', '/page/test/index', 4, 1, '1', '测试', 'iconfont icon-fenlei', '2021-03-02 16:04:52');
INSERT INTO `fx_auth_rule` VALUES (47, 42, '修改页面', '/page/test/edit', 1, 1, '0', '测试修改', 'iconfont icon-gouxuan1', '2021-03-02 16:08:19');
INSERT INTO `fx_auth_rule` VALUES (48, 42, '单纯列表', '/page/test/listonly', 4, 1, '1', '', 'iconfont icon-gouxuan1', '2021-03-15 15:21:59');
INSERT INTO `fx_auth_rule` VALUES (50, 42, '列表-修改-删除', '/page/test/list', 3, 1, '1', '列表2', 'iconfont icon-gouxuan1', '2021-03-17 11:19:33');

-- ----------------------------
-- Table structure for fx_role
-- ----------------------------
DROP TABLE IF EXISTS `fx_role`;
CREATE TABLE `fx_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `is_super` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否超级管理员',
  `rule_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '菜单id 用,隔开',
  `api_ids` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'api接口id',
  `role_desc` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `uid` int(11) NULL DEFAULT NULL COMMENT '创建人id',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态 1正常',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_role
-- ----------------------------
INSERT INTO `fx_role` VALUES (1, 'superadmin', '超级管理员', 1, '', NULL, '超级管理员拥有所有权限 这个角色是由系统定的，不要更改', 0, 1, NULL, NULL);
INSERT INTO `fx_role` VALUES (6, NULL, 'role1', 0, '38,45,47', '20008,20009,20010,20023,20025,20028', 'role1', 1, 1, '2021-03-15 14:57:17', '2021-03-15 15:33:20');
INSERT INTO `fx_role` VALUES (7, NULL, 'user1-1', 0, '37,38,40', '20003,20006,20008,20016,20019,20020', 'user1-1', 2, 1, '2021-03-15 15:13:01', '2021-03-15 15:13:23');

-- ----------------------------
-- Table structure for fx_test
-- ----------------------------
DROP TABLE IF EXISTS `fx_test`;
CREATE TABLE `fx_test`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `image` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图片',
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '多张图片 json',
  `img` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '单图',
  `imgs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '多图 json',
  `pic` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '单图',
  `pics` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '多图 json',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容 富文本',
  `state` tinyint(2) NOT NULL COMMENT '状态:0=关闭,1=开启',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_test
-- ----------------------------
INSERT INTO `fx_test` VALUES (3, '111111111', 'http://fxadmin.com/data/upload/20210303//20210303104605_393.png', '[\"http://fxadmin.com/data/upload/20210303//20210303104605_393.png\",\"http://fxadmin.com/data/upload/20210302//20210302104637_182.png\",\"http://fxadmin.com/data/upload/20210302//20210302103413_729.jpg\"]', 'http://fxadmin.com/data/upload/20210303//20210303104605_393.png', '[]', 'http://fxadmin.com/data/upload/20210303//20210303104605_393.png', '[]', '<p>232323232323233223</p>', 0, '2021-03-03 12:01:06');
INSERT INTO `fx_test` VALUES (4, 'rrttyyuuiioopp', 'http://fxadmin.com/data/upload/20210302//20210302103355_454.png', '[\"http://fxadmin.com/data/upload/20210302//20210302104637_182.png\",\"http://fxadmin.com/data/upload/20210302//20210302140100_193.jpg\",\"http://fxadmin.com/data/upload/20210302//20210302114758_464.jpg\",\"http://fxadmin.com/data/upload/20210303//20210303104605_393.png\"]', 'http://fxadmin.com/data/upload/20210302//20210302103355_454.png', '[]', 'http://fxadmin.com/data/upload/20210302//20210302103355_454.png', '[]', '<p>sdfg34fgsdfert3gerefgdfgsdf塔顶地</p><p><img src=\"http://fxadmin.com/data/upload/20210302//20210302103413_729.jpg\"></p>', 0, '2021-03-03 14:23:01');
INSERT INTO `fx_test` VALUES (5, '会顶替顶替', 'http://fxadmin.com/data/upload/20210302//20210302111453_514.png', '[\"http://fxadmin.com/data/upload/20210302//20210302103413_729.jpg\",\"http://fxadmin.com/data/upload/20210302//20210302103408_446.png\"]', 'http://fxadmin.com/data/upload/20210302//20210302111453_514.png', '[\"http://fxadmin.com/data/upload/20210302//20210302104637_182.png\",\"http://fxadmin.com/data/upload/20210302//20210302103650_911.jpg\"]', 'http://fxadmin.com/data/upload/20210302//20210302111453_514.png', '[\"http://fxadmin.com/data/upload/20210302//20210302110922_653.jpg\",\"http://fxadmin.com/data/upload/20210302//20210302111453_514.png\"]', '<p><img src=\"http://fxadmin.com/data/upload/20210302//20210302103408_446.png\"></p><p>防守打法硒鼓</p>', 0, '2021-03-17 11:59:13');

-- ----------------------------
-- Table structure for fx_user
-- ----------------------------
DROP TABLE IF EXISTS `fx_user`;
CREATE TABLE `fx_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_type` int(11) NULL DEFAULT 1 COMMENT '1平台管理员',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父级id',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `auth_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '令牌',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1正常 0禁用',
  `avatar` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `creater_id` int(11) NULL DEFAULT 0 COMMENT '创建人id',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '上次登录时间',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fx_user
-- ----------------------------
INSERT INTO `fx_user` VALUES (1, 1, 1, 0, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'vk590_7iE3xxxhPZ1lRSa4MlCQb95Fvo', 1, 'http://fxadmin.com/data/upload/20210302//20210302114546_271.jpg', 0, '2021-03-17 14:35:21', '2021-03-17 14:35:00', NULL, '2021-03-15 14:15:33');
INSERT INTO `fx_user` VALUES (2, 6, 1, 1, 'user1', 'e10adc3949ba59abbe56e057f20f883e', '5h98IUCZNcFBIHGfeIjgIdPkwbWAuVEh', 1, NULL, 1, '2021-03-15 15:37:08', '2021-03-15 15:33:27', '2021-01-08 22:28:40', '2021-03-15 14:57:57');
INSERT INTO `fx_user` VALUES (4, 5, 1, 3, '子user', 'e10adc3949ba59abbe56e057f20f883e', 'xdCdNJiilaTWkU09D67KXr8UUFC7MH7P', 1, NULL, 3, '2021-03-15 14:40:46', '2021-03-15 14:40:46', '2021-03-15 14:39:44', NULL);
INSERT INTO `fx_user` VALUES (5, 7, 1, 2, 'user1-1', 'e10adc3949ba59abbe56e057f20f883e', 'exrvm8t5d3y2vHP-Hts2RLfYjl3qPGE9', 1, NULL, 2, '2021-03-15 15:13:51', '2021-03-15 15:13:51', '2021-03-15 15:13:38', NULL);

SET FOREIGN_KEY_CHECKS = 1;
