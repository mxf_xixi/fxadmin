<?php
namespace admin\v1;

/**
 * v1 module definition class
 */
class module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'adminv1';

    /**
     * @inheritdoc
     */
    public function init()
    {
        
        parent::init();
        
        // custom initialization code goes here 
    } 
}
