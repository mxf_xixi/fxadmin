<?php
namespace adminv1;
use common\models\AlbumClass;

class AlbumClassController extends CoreController
{
    protected function getModelClass(): string
    {
        return AlbumClass::class;
    }


}
