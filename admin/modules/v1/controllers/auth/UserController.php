<?php
namespace adminv1\auth; 
use adminv1\CoreController;
use Yii;
use admin\models\auth\User;
use admin\models\auth\Roles;
/*
	用户相关控制器
*/

class UserController extends CoreController
{

    protected function getModelClass(): string
    {
        return User::class;
    }


	//角色列表
	public function actionList()
	{
		$where = ['creater_id'=>$this->_uid];
		$list = User::getUserList($where);
		$roles = Roles::getRoles($this->_user);
		$roleList = array();
		foreach($roles as $val){
			$roleList[$val['id']] = $val['role_name'];
		}
		return $this->success('用户列表',$list, array('role_list' => $roleList));
	}

	/*用户添加 修改*/
	public function actionUserEdit()
	{
		$user = new User;
		$params = $this->request;
		if($id=$this->request('id')){
			$user = $user::findOne($id);
			$params['update_time'] =  $this->curDateTime;
		}else{
			$params['pid'] = $this->_uid;
			$params['creater_id']  = $this->_uid;
			$params['create_time']  = $this->curDateTime;
		}	

		if($this->request('password'))
			$params['password'] = md5($this->request('password'));
		else
			unset($params['password']);


		if($user->load($params, '') && $user->validate() && $user->save())
		{	
			if(!$params['id']) $user->id = Yii::$app->db->getLastInsertID();
			return $this->success('操作成功', $user->attributes);
			$errMsg = $user::outError($user->getErrors());
			return $this->error($errMsg['msg']);

		}else{
			$error = $user->getErrors();
			$errMsg = $user::outError($user->getErrors());
			return $this->error($errMsg);
		}		
	}

	//删除
	public function actionUserDel()
	{
		if(!$id = $this->request('id'))
			return $this->error('参数错误');

		$where = ['id'=>$id];
		$res = User::deleteAll($where);
		if($res) return $this->success('删除成功');
		return $this->error('删除失败');

	}


	//获取用户信息
	public function actionUserInfo()
	{
		$data = [
			'username' => $this->_user['username'],
			'lastLogin' => $this->_user['last_login_time'],
			'id'		=> $this->_uid,
			'avatar'	=> $this->_user['avatar'],
		];
		return $this->success('用户信息',$data);
	}

	//信息修改
	// old_pass
	// new_pass
	// username
	public function actionEditInfo()
	{
		
		if(!$id = $this->request('id')) $this->error('参数错误');

		$user = new User;
		$user = $user::findOne($id);

		//用户名修改
		if($this->request('username')){
			$user->username = $this->request('username');
		}

		//密码修改
		if($this->request('old_pass') && $this->request('new_pass'))
		{
			if(md5($this->request('old_pass'))!=$this->_user['password'])
				return $this->error('密码错误');		
	
			$user->password  = md5($this->request('new_pass'));			
		}

		//头像修改
		if($this->request('avatar')){
			$user->avatar = $this->request('avatar');
		}

		$user->update_time = $this->curDateTime;
		if($user->save(false)) return $this->success('修改成功');
		return $this->error('修改失败');
	}



}