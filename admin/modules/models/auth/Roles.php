<?php
/*
	平台后台管理员角色相关模型
*/
namespace admin\models\auth;
use Yii;
use common\models\Role as FxRole;

class Roles extends FxRole
{

	//获取角色列表
	public static function getRoles($user)
	{
		$where = ['uid'=>$user['id']];
		$list = self::find()->where($where)->asarray()->all();
		return $list;
	}

	/*
		角色api权限
	*/
	public static function getRolesRules($roleId,$isTree=false,$type=0)
	{
		$role = self::findOne(['id'=>$roleId]);
		if($roleId == '1'){
			//这里定死角色id为1的 是超级管理员
			$where = ['>', 'id','0'];
		}else{
			$ruleIds = explode(',', $role['rule_ids']);
			$where = ['in','id',$ruleIds];			
		}

		$data = AuthRule::getAuthRules($where,$type);	
		if($isTree) $data = self::getTree($data);
		return $data;	
	}


	/*
		角色页面菜单权限
	*/
	public static function getRolesApi($roleId,$isTree=false)
	{
		$role = self::findOne(['id' => $roleId]);
		if($roleId == '1'){
			//这里定死角色id为1的 是超级管理员
			$where = ['>', 'id', '0'];
		}else{
			$apiIds = explode(',', $role['api_ids']);
			$where = ['in', 'id', $apiIds];			
		}
		$data = AuthApi::find()->where($where)->asarray()->all();	
		return $data;
	}

	/*
		角色权限  树状结构的
	*/
	public static function getRolesRulesTree($roleId,$type=0)
	{

		$role = self::findOne(['id'=>$roleId]);
		if($roleId == '1'){
			//这里定死角色id为1的 是超级管理员
			$where = ['and',"is_menu='1'",['>','id','0']]; 
		}else{
			$ruleIds = explode(',', $role['rule_ids']);
			$where = ['and',"is_menu='1'",['in','id',$ruleIds]];
		}

		$data = AuthRule::getAuthRules($where,$type);
		$rules = self::getTree($data);
		return $rules;

	}

	public static function getTree($items,$pid='pid')
	{

    	$map  = [];
    	$tree = [];   
    	foreach ($items as &$it)
    	{ 
    		//数据的ID名生成新的引用索引树
    		$map[$it['id']] = &$it; 
    	}  
    	foreach ($items as &$it)
    	{
	        $parent = &$map[$it[$pid]];

	        //选中的子菜单，但没有父级菜单
	        if($it[$pid]>0 && !$parent){
	        	$parentMenu = self::getParent($it[$pid]);
	        	$map[$parentMenu['id']] = $parentMenu;
	        	$tree[] = &$map[$it[$pid]];//$parentMenu;
	        	$parent = &$map[$it[$pid]];
	        }

	        if($parent) {
	            $parent['children'][] = &$it;
	        }else{
	            $tree[] = &$it;
	        }
    	}	
    	return $tree;
	}

	public static function getParent($id)
	{
		return AuthRule::getAuthRule($id);
	}


	//显示角色权限, 并且将其可以添加的权限都显示出来,即此角色上级的所有权限 
	public static function showRoleAuth($roleId)
	{
		//当前角色权限
		$role = self::findOne(['id' => $roleId]);
		$ruleIds = explode(',', $role['rule_ids']);
		$apiIds = explode(',',  $role['api_ids']);
		$curRules = array_merge($ruleIds, $apiIds);

		//此角色的创建人
		$createUser = User::getUser(['id'=>$role['uid']]);
		$createRole = self::findOne(['id'=>$createUser['role_id']]);

		//获取创建人所拥有的权限
		if($roleId == '1' || $createUser['role_id'] == 1){
			//这里定死角色id为1的 是超级管理员
			$where = ['>','id','0'];
			$apiIds = [];
		}else{

			$ruleIds = explode(',', $createRole['rule_ids']);
			$where = ['in', 'id', $ruleIds];	
			$apiIds = explode(',', $createRole['api_ids']);
		}
		$data = AuthRule::getAuthRules2($where,$apiIds);	
		$authRuleTree = self::getTree($data);

		//返回所有可显示的权限  和 当前角色所拥有的权限
		return array('auth_tree'=>$authRuleTree,'role_auth'=>$curRules);
	}
	

	//修改角色权限
	public static function editRoleAuth($id,$data)
	{
		return self::updateAll($data,['id' => $id]);
	}

}

