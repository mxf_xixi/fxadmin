<?php
return [
    'adminEmail' => '2252599083@qq.com',
    'mobileCodenum' => '5',  //次
    'mobileCodeExpire' => '1800', //秒
   
    ## 是否开启接口验证, 开启的话，需要注意fx_auth_api 与 fx_auth_rule的主键 不能相同，fx_auth_api 的主键 >= 20000
    ## 修改之后，请重新登录
    'apiAuth' => true, 
    ## 开启接口验证后，需要忽略验证的接口
    'ignore_api' => ['v1/auth/user/user-info', 'v1/auth/user/edit-info', 'v1/common/upload'],


];
