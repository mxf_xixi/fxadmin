<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/params.php'));
define('TIME', time());
$config = [
    'id' => 'app-apiadmin',
    'defaultRoute' => 'v1/',
    'basePath' => dirname(__DIR__),
    'modules' => [ // 添加模块v1和v2，分别表示不同的版本
        'v1' => [
            'class' => 'admin\v1\module'
        ],

    ],
    'controllerNamespace' => 'adminv1',
    'components' => [
        'request' => [
            'cookieValidationKey' => 'w3BnewAWmCrjijzkiLucYD5Ty1Ym_V9F'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true, // 美化url==ture
            'rules' => [
            ]
        ],
    ],
    'as cors' => [
        'class' => yii\filters\Cors::class,
        'cors' => [
            'Origin' => ['*'],
            'Access-Control-Allow-Credentials' => false,
        ],
    ],
    'params' => $params
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '222.244.77.25', '192.168.1.*', '172.17.0.1', '::1'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '222.244.77.25', '192.168.1.*', '172.17.0.1', '::1'],
        'generators' => [
            'module' => [
                'class' => 'yii\gii\generators\module\Generator',
            ],
            'model' => [
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'mymodel' => '../../common/giiTemplate/model',   ## 自定义模型生成模板
                ]
            ],
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'mycrud' => '../../common/giiTemplate/crud',   ## 自定义模型生成模板
                ]
            ],
        ],
    ];
}
return $config;
