<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '4ef2fd7bc71a67a123c80a0d20e7eb93d26245f3',
    'name' => 'yiisoft/yii2-app-advanced',
  ),
  'versions' => 
  array (
    'behat/gherkin' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '51ac4500c4dc30cbaaabcd2f25694299df666a31',
    ),
    'bower-asset/bootstrap' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '68b0d231a13201eb14acd3dc84e51543d16e5f7e',
    ),
    'bower-asset/inputmask' => 
    array (
      'pretty_version' => '3.3.11',
      'version' => '3.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
    ),
    'bower-asset/jquery' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '15bc73803f76bc53b654b9fdbbbc096f56d7c03d',
    ),
    'bower-asset/punycode' => 
    array (
      'pretty_version' => 'v1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'pretty_version' => '2.0.7.1',
      'version' => '2.0.7.1',
      'aliases' => 
      array (
      ),
      'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bac5e971dd391e2802dca5400bbeacbaea9eb86',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.5',
      'version' => '4.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '24f2345329b1059f1208f65581fc632a4a6e5a55',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acd0dc8b394595a74b58dcc889f72569ff7d8e71',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2123542b1325cc349ac68868abe74638bcb32ab6',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '79f13d05b63f2fceba4d0e78044bab668c9b2a6b',
    ),
    'codeception/module-filesystem' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe3c352479924ec0aaf6a6c3d6825dc14242b81e',
    ),
    'codeception/module-yii2' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '89ec2b34db0fe1c80d404d939ecc7ab07051b4b7',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '8.1.2',
      'version' => '8.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e610200adf75ebc1ea7cf10d7cdb43e0f5fff3cc',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.6.1',
      'version' => '3.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a3ba01414cbee76a1bced9f9b6b169cc8d203880',
    ),
    'codeception/verify' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0bb946b6d61279f461bcc5a677ac0ed5eab9b3',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae466f726242e637cebdd526a7d991b9433bacf1',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.17',
      'version' => '2.1.17.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ade6887fd9bd74177769645ab5c474824f8a418a',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.12.0',
      'version' => '4.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a617e55bc62a87eec73bd456d146d134ad716f03',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.9.5',
      'version' => '1.9.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2c28789e80a97badd14145fda39b545d83ca3ef',
      'replaced' => 
      array (
        0 => '1.9.5',
      ),
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.5.3',
      'version' => '3.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cac47092144043d5d676e2e7cf8d0d2f83fc89ca',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6568f4687e5b41b054365f9ae03fcb1ed5f2069b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.1.0',
      'version' => '5.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd72d394ca794d3466a3b2fc09d5a6c1dc86b47e',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7462d5f123dfc080dfdf26897032a6513644fc95',
    ),
    'phpoffice/phpexcel' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1441011fb7ecdd8cc689878f54f8b58a6805f870',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0464787bfa7cd13576c5a1e318709768798bec6a',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'v1.10.3',
      'version' => '1.10.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '451c3cd1418cf640de218914901e51b064abb093',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.10',
      'version' => '7.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1884187926fbb755a9aaf0b3836ad3165b478bf',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.5',
      'version' => '8.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '63dda3b212a0025d380a745f91bdb4d8c985adb7',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edf8a461cf1d4005f19fb0b6b8b95a9f7fa0adc4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3aaaa15fa71d27650d62a948be022fe3b48541a3',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '61d85c5af2fc058014c7c89504c3944e73a086f0',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa1caadc8cdaa17bcfb25219f3b53fe294a9935',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f8d5271303dad260692ba73dfa21777d38e124e',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => 'v4.4.8',
      'version' => '4.4.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d0fb3374324071ecdd94898367a3fa4b5563162',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '24f40d95385774ed5c71dbf014edd047e2f2f3dc',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '405952c4e90941a17e52ef7489a2bd94870bb290',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '600a52c29afc0d1caa74acbec8d3095ca7e9910d',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e94c8b1bbe2bc77507a1056cdb06451c75b427f9',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4de7601eefbf25f9d47190abe07f79fe0a27424',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3bff59ea7047e925be6b7f2059d60af31bb46d6a',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa79b11539418b02fc5e1897267673ba2c19419c',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f048e612a3905f34931127360bdd2def19a5e582',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a760d8964ff79ab9bf057613a5808284ec852ccc',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '66a8f0957a3ca54e4f724e49028ab19d75a8918b',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.0.8',
      'version' => '5.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '482fb4e710e5af3e0e78015f19aa716ad953392f',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '11336f6f84e16a720dae9d8e6ed5019efa85a0f9',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab2cb0b3b559010b75981b1bdce728da3ee90ad6',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => '2.0.35',
      'version' => '2.0.35.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd42809e4969cdc0adb97197ba32774b3e4cd9e8e',
    ),
    'yiisoft/yii2-app-advanced' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '4ef2fd7bc71a67a123c80a0d20e7eb93d26245f3',
    ),
    'yiisoft/yii2-bootstrap' => 
    array (
      'pretty_version' => '2.0.10',
      'version' => '2.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '073c9ab0a4eb71f2485d84c96a1967130300d8fc',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => '2.0.9',
      'version' => '2.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd191176c4f8372e397a9e3df27360dca6a70efaa',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => '2.1.13',
      'version' => '2.1.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '696712a2a3565b1a072ad3c9d298e262967e8282',
    ),
    'yiisoft/yii2-faker' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3df62b1dcb272a8413f9c6e532c9d73f325ccde1',
    ),
    'yiisoft/yii2-gii' => 
    array (
      'pretty_version' => '2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd879cb186361fbc6f71a2d994d580b5a071a5642',
    ),
    'yiisoft/yii2-swiftmailer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '09659a55959f9e64b8178d842b64a9ffae42b994',
    ),
  ),
);
