
# fxadmin后台管理
fxadmin 是一个yii2 + vue + element-ui的后台极速开发框架，前后端分离。 可通过gii实现自动生成页面（支持文本、富文本、单图、多图、生成），快速开发。
基础功能有： 权限管理（实现了页面路由 + 接口权限控制, 接口权限可随意关闭）、图片相册、图片上传、富文本、导出。

## 如何使用

### 安装教程

#### 下载
1. `clone https://gitee.com/mxf_xixi/fxadmin.git`
2. `cd fxadmin`

#### 接口服务部署
1. 安装依赖，执行 `composer install`  下载需要的依赖。 如果下载不下来可直接解压vendor.zip
2. 导入数据库，创建fxadmin_db数据库 将fxadmin.sql导入数据库中
3. 配置接口域名，将配置的域名指向fxadmin/admin/web目录  假如域名为fxadmin.com (此时直接访问此域名是没内容的这只是作为接口)

#### 前端vue项目部署
1. cd vueadmin
2. 执行npm install 
	1. install失败  1、删除node_modules 2、npm cache clean --force 3、npm install
	2. install失败  1、删除node_modules 2、删除package-lock.josn   3、npm install

3. 修改接口域名，将vueadmin/src/components/js/request.js里的域名换成上面配置好的域名
4. 安装成功后 执行 npm run dev  打开http://localhost:8080 即可
5. 打包 执行npm run build  会生成一个dist文件  将dist文件放在fxadmin/admin/web下， 此时访问fxadmin.com/dist/


### 生成页面
上面配置好之后  打开 http://fxadmin.com/index.php/gii 或 http://fxadmin.com/gii  到gii页面

#### 生成Model 

	如表fx_test  
	类名即为Test
	Namespace 填  common\models
	勾选 Use Table Prefix 、 Generate Labels from DB Comments 、 Use Schema Name
	模板 Code Template  选择我们自定义的模板 mymodel 


#### 生成 CRUD Generator
	model class 填 common\models\Test
	Controller Class  填 adminv1\TestController
	view path  填 @vue/src/page/test	
	模板 Code Template  选择我们自定义的模板 mymodel
	点击生成后，会在对应目录下生成文件，此时先将index.php、 list.php等 改成.vue文件 保存， 然后格式化一下


#### 创建菜单
	在菜单权限--》 菜单列表 添加对应的路由，保存后，重新登录即可看到
	菜单分为路由 和 接口 需要填写对应的内容， 
	如果不需要接口权限，在admin/config/params.php 里关闭即可


