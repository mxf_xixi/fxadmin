<?php

namespace common\service;
use common\models\goods\GoodsCate;

class GoodsCateService 
{

	//获取分类树结构
   	public static function getCateTree($pid='0')
   	{
  		$cates = GoodsCate::find()->where(['is_delete'=>0])->asArray()->all();
  		$data  = self::getTree($cates,$pid);
  		return $data;
   	}

   	//格式化分类
   	protected static function  cateTree($cates,$parentId=0,&$list=array(),$level=0)
   	{
   		foreach($cates as $cate)
   		{
   			if($cate['parent_id']==$parentId)
   			{
   				$cate['level'] = $level;
   				$list[] = $cate;
   				self::cateTree($cates,$cate['id'],$list,$level+1); 
   			}
   		}
   		return $list;
   	}


	protected static function getTree($data, $parentId = 0)
	{
	    $tree = array();
	    foreach ($data as $k => $v) 
	    {
	        if ($v["parent_id"] == $parentId) 
	        {
	            unset($data[$k]);
	            if (!empty($data)) 
	            {
	                $children = self::getTree($data, $v["id"]);
	                if (!empty($children)) 
	                {
	                    $v["children"] = $children;
	                }
	            }
	            $tree[] = $v;
	        }
	    }
	    return $tree;
	}


}