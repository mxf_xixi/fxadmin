<?php
/**
 * This is the template for generating a controller class file.
 */
use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\controller\Generator sssss*/

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>
namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;
use <?= ltrim($generator->modelClass, '\\') ?>;

class <?= StringHelper::basename($generator->controllerClass) ?> extends CoreController
{

protected function getModelClass(): string
{
return <?= $modelClass ?>::class;
}
}
