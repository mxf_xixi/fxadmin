<?php 
use yii\helpers\StringHelper;
$tableSchema = $generator->getTableSchema();
$controllerClass = StringHelper::basename($generator->controllerClass);
$controller = str_replace('Controller', '', $controllerClass);
$controller = lcfirst($controller);
$controller = toUnderScore3($controller);

//驼峰命名转中划线命名
function toUnderScore3($str)
{
  $dstr = preg_replace_callback('/([A-Z]+)/',function($matchs)
  {
      return '-'.strtolower($matchs[0]);
  },$str);
  return trim(preg_replace('/-{2,}/','-',$dstr),'-');
}
?>

<template>
    <div>
        <div class="crumbs">
            <el-breadcrumb separator="/">
                <el-breadcrumb-item><i class="el-icon-lx-calendar"></i>一级</el-breadcrumb-item>
                <el-breadcrumb-item>二级</el-breadcrumb-item>
            </el-breadcrumb>
        </div>
        <div class="container">
            <div class="search">
                <el-row type="flex" :gutter="20">
                    <el-col :span="1">
                         <el-button type="success" icon="el-icon-refresh" circle @click="getData"></el-button>
                    </el-col>
<?php
    
    $stateListArr = [];
    $searchKeyList = [];
    foreach ($tableSchema->columns as $column)
    {
        if($column->name == 'state') 
        {
            $stateListArr['field'] = $column->name;
            $stateTextArr = explode(":",  $column->comment);
            if(count($stateTextArr) <= 1) continue;
            $stateName = $stateTextArr['0'];
            $stateValArr = explode(',', $stateTextArr[1]);
            $stateListArr['name'] = $stateName;

            foreach($stateValArr as $valArr) {
                $tmpValArr = explode('=', $valArr);
                if(count($tmpValArr) < 2) continue;
                $key = $tmpValArr[0];
                $value = $tmpValArr[1];
                $stateListArr['data'][] = ['key' => $key, 'value' => $value];
            }
            $searchKeyList[] = $column->name;
        }


    }

?>
<?php if($stateListArr && !empty($stateListArr['data'])) {?>
                    <el-col :span="3">
                        <el-select v-model="search.<?=$stateListArr['field']?>" placeholder="<?=$stateListArr['name']?>" @change="getData">
                            <el-option
                              v-for="item in stateText"
                              :key="item.key"
                              :label="item.label"
                              :value="item.key">
                            </el-option>
                        </el-select>
                    </el-col>
<?php }?>
                    <el-col :span="2">
                        <el-button :loading="loading" type="primary" @click="searchRes" icon="el-icon-search">
                            搜索
                        </el-button>
                    </el-col>
                    <el-col>
                        <el-button :loading="loading" type="danger" @click="exportExecl" icon="el-icon-download">
                            导出
                        </el-button>
                    </el-col>
                </el-row>             
            </div>
            <el-table
                :data="list"
                border
                v-loading="loading">

<?php 
foreach ($tableSchema->columns as $column): ?>
<?php
    $prop = $column->name;
    $label = $column->comment ? $column->comment : strtoupper($column->name);
?>
<?php 
    
    $isImg = false;
    $match = '/(img)|(image)|(logo)|(pic)|(picture)/';
    if(preg_match($match, $prop)) $isImg = true;

    if((strpos($column->comment, 'json') !== false)) {
        continue;
    }
    
    $match = '/(imgs)|(images)|(pics)|(pictures)/';
    if(preg_match($match, $prop)) {
        continue;  
    } 

    if($isImg) {
?>
                <el-table-column
                    prop="<?=$prop?>"
                    label="<?=$label?>"
                    align="center">
                    <template slot-scope="scope">
                        <el-image 
                            style="width: 100px; height: 100px"
                            :src="scope.row.<?=$prop?>" 
                            :preview-src-list="[scope.row.<?=$prop?>]">
                        </el-image>
                    </template>
                </el-table-column>

<?php }else{?>
                <el-table-column
                    prop="<?=$prop?>"
                    label="<?=$label?>"
                    align="center">
                </el-table-column>
<?php }?>
<?php endforeach; ?>

                <el-table-column
                    align="center"
                    label="操作">
                    <template slot-scope="scope">
                    	--
                    </template>
                </el-table-column>
            </el-table>

            <div class="pagination">
                <el-pagination
                    background
                    @size-change="handleSizeChange"
                    @current-change="handleCurrentChange"
                    :current-page="page"
                    :page-sizes="[20, 50, 100, 200]"
                    :page-size="pageSize"
                    layout="total, sizes, prev, pager, next"
                    :total="totalNums">
                </el-pagination>
            </div>
        </div>
    </div>
</template>

<script type="text/javascript">
export default{
    data() {
        return {
            loading: true,

            list: [],

            page: 1,
            pageSize: 20,
            totalNums: 0,

<?php
    if($stateListArr && !empty($stateListArr['data'])) {
        $str = "stateText:[\r\n";
        foreach($stateListArr['data'] as $val){
            $str .= "{key:".$val['key'].", label:'".$val['value']."'},\r\n";
        }
        $str .= "],";
        echo $str;
    }
?>


            search: {
<?php 
    if($searchKeyList) {
        foreach($searchKeyList as $val) {
            echo "               ".$val.": '', \r\n";
        }
    }

?>
            }
        }
    },
    created() {
        this.getData();
        
    },

    methods:{
        getData() {
            let params = {
                page: this.page,
                page_size: this.pageSize,
                search: JSON.stringify(this.search),
            }
            this.loading = true;
            this.$post_('<?=$controller?>/list', params, (res) => {
                this.loading = false;
                if(res.code == '0'){
                    this.list = res.data;
                    this.totalNums = Number(res.extend.totalNums);
                }else{
                    this.$message.warning(res.msg);
                }
            }, (res) => {
                this.loading = false;
                this.$message.error('出错了！')
            });
        },
        // 分页导航
        handleCurrentChange(val) {
            this.page = val;
            this.getData();
        },
        handleSizeChange(val) {
            this.pageSize = val;
            this.page = 1;
            this.getData();
        },
        //搜索
        searchRes() {
            this.page = 1;
            this.getData();
        },
        //导出
        exportExecl() {
            this.loading = true;
            let params = {
                search: JSON.stringify(this.search),
            }
            this.$post_('<?=$controller?>/export', params, (res) => {
                if(res.code=='0'){
                    this.$download(res.data.url);
                }else{
                    this.$message.error(res.msg);
                }
                this.loading = false;
            })
        }

    }
}
</script>

<style type="text/css" scoped>
    .search{
        margin-bottom: 10px;
    }
</style>