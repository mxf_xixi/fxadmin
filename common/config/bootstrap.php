<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@adminv1', dirname(dirname(__DIR__)) . '/admin/modules/v1/controllers');  ## 定位到v1
Yii::setAlias('@admin',dirname(dirname(__DIR__)) . '/admin/modules');
Yii::setAlias('@apiv1', dirname(dirname(__DIR__)) . '/api/modules/v1/controllers');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api/modules');
Yii::setAlias('@vue', dirname(dirname(__DIR__)) . '/vueadmin');