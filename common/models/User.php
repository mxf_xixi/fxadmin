<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class User extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%user}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['role_id', 'username'], 'required'],
            [['role_id', 'user_type', 'pid', 'status', 'creater_id'], 'integer'],
            [['login_time', 'last_login_time', 'create_time', 'update_time'], 'safe'],
            [['username', 'password', 'auth_key'], 'string', 'max' => 32],
            [['avatar'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'user_type' => '1平台管理员',
            'pid' => '父级id',
            'username' => '用户名',
            'password' => 'Password',
            'auth_key' => '令牌',
            'status' => '状态 1正常 0禁用',
            'avatar' => '头像',
            'creater_id' => '创建人id',
            'login_time' => '登录时间',
            'last_login_time' => '上次登录时间',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }

}


