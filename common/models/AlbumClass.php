<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class AlbumClass extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%album_class}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['class_name'], 'required'],
            [['update_time'], 'safe'],
            [['is_default'], 'integer'],
            [['class_name'], 'string', 'max' => 100],
            [['class_cover'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => '相册id',
            'class_name' => '相册名称',
            'class_cover' => '相册封面',
            'update_time' => '创建时间',
            'is_default' => '是否为默认相册,1代表默认',
        ];
    }

}


