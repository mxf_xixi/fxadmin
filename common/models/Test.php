<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class Test extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%test}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'image', 'state'], 'required'],
            [['images', 'imgs', 'pics', 'content'], 'string'],
            [['state'], 'integer'],
            [['create_time'], 'safe'],
            [['title'], 'string', 'max' => 64],
            [['image', 'img', 'pic'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'image' => '图片',
            'images' => '多张图片 json',
            'img' => '单图',
            'imgs' => '多图',
            'pic' => '单图',
            'pics' => '多图',
            'content' => '内容 富文本',
            'state' => '状态:0=关闭,1=开启',
            'create_time' => '创建时间',
        ];
    }

}


