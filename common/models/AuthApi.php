<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class AuthApi extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%auth_api}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['rule_id', 'name', 'api_url'], 'required'],
            [['rule_id'], 'integer'],
            [['create_time'], 'safe'],
            [['name'], 'string', 'max' => 32],
            [['api_url'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'name' => '接口名称',
            'api_url' => '接口路由',
            'create_time' => 'Create Time',
        ];
    }

}


