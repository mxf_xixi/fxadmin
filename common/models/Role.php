<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class Role extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%role}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['is_super', 'uid', 'status'], 'integer'],
            [['rule_ids', 'api_ids'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['code'], 'string', 'max' => 32],
            [['role_name', 'role_desc'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => '角色ID',
            'code' => 'Code',
            'role_name' => '角色名称',
            'is_super' => '是否超级管理员',
            'rule_ids' => '菜单id 用,隔开',
            'api_ids' => 'api接口id',
            'role_desc' => '角色描述',
            'uid' => '创建人id',
            'status' => '状态 1正常',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }

}


