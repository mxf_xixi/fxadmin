<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class AuthRule extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%auth_rule}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['pid', 'sort', 'type'], 'integer'],
            [['create_time'], 'safe'],
            [['name', 'icon'], 'string', 'max' => 32],
            [['path'], 'string', 'max' => 128],
            [['is_menu'], 'string', 'max' => 1],
            [['desc'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'pid' => '父id',
            'name' => '名称',
            'path' => '路径',
            'sort' => '排序 ',
            'type' => '1平台路由',
            'is_menu' => '是否为菜单 1是',
            'desc' => '描述',
            'icon' => '图标',
            'create_time' => 'Create Time',
        ];
    }

}


