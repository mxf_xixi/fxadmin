<?php
/*
	基础模型
*/
namespace common\models;
use \Yii;

class BaseModel extends \yii\db\ActiveRecord
{
    //总记录数
    public static $totalNums;
    public static $query;

    /*
        组装查询 针对是数据的分页查询
        $model  对象
        $where  条件
        $params  参数
    */
    public static function queryFormart($model,$where,$params,$whereAnd=array()): array
    {
        $order = '';

        $field = $params['field'] ?? ['*'];

        if(isset($params['order']))
            $order = $params['order'];

        //分页
        if(isset($params['page']) && $params['page'] > 0){
            $page   = $params['page'];
            $limit  = $params['limit'] ? $params['limit'] : 10;
            $offset = ($page - 1) * $limit;
        }else{
            $offset = '';
            $limit  = '';
        }
        $model = $model->select($field)->where($where);

        if($whereAnd)
        {
            foreach($whereAnd as $and){
                $model = $model->andFilterWhere($and);
            }
        }

        //总数量
        $totalNums = $model->count();
        $query = $model;


        $models = $model->orderBy($order)->offset($offset)->limit($limit);

        return array(
            'model'	=> $models,
            'totalNums'	=> $totalNums ? $totalNums : 0,
            'query' => $query,
        );
    }


    //返回错误
    public static function outError($errors)
    {
        if(!is_array($errors)) return 'error model';
        $firstError = array_shift($errors);
        if(!is_array($firstError)) return 'error model';
        return array_shift($firstError);
    }


    /*
		* data list
		* @param whereArr 条件
		* @param params 基本参数 包含 field order page limit
		* @param extends  扩展信息 一些相关的信息
		* return array
	*/
    public static function dataList($whereArr, $params, $extends = array()): array
    {
        $model  = self::find();
        $where  = $whereArr['where'] ?? [];
        $whereAnd = $whereArr['and'] ?? [];
        $models = self::queryFormart($model, $where, $params, $whereAnd);
        $model  = $models['model'];
        self::$totalNums = $models['totalNums'];
        self::$query = $models['query'];

        $data  = $model->asarray()->all();
        if(empty($data)) return array();
        if(empty($extends)) return $data;
        foreach($extends as $ext)
        {
            ## 获取需要的扩展信息

        }
        return $data;
    }


    public static function getOne($where, $field = ['*'])
    {
        return self::find()->select($field)->where($where)->one();
    }


    public static function getAll($where, $field = ['*']): array
    {
        return self::find()->select($field)->where($where)->all();
    }

    public static function getOneArr($where, $field = ['*'])
    {
        return self::find()->select($field)->where($where)->asArray()->one();
    }

    public static function getAllArr($where, $field = ['*']): array
    {
        return self::find()->select($field)->where($where)->asArray()->all();
    }

}
