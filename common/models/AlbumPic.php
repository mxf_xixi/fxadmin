<?php
namespace common\models;
use Yii;
use common\models\BaseModel;


class AlbumPic extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%album_pic}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['class_id'], 'integer'],
            [['update_time'], 'safe'],
            [['image_url'], 'string', 'max' => 200],
            [['image_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'class_id' => '分类id',
            'image_url' => '图片路径',
            'image_name' => 'Image Name',
            'update_time' => 'Update Time',
        ];
    }

}


