import axios from 'axios'

//接口域名
// var postUrl = 'http://fxadmin.com:7889/v1/'; //tinakj
// export let baseUrl  = 'http://fxadmin.com:7889/v1/';
// export let uploadUrl = 'http://fxadmin.com:7889/v1/common/upload';

var postUrl = 'http://fxadmin.com:180/index.php/v1/'; //tinakj
export let baseUrl  = 'http://fxadmin.com:180/index.php/v1/';
export let uploadUrl = 'http://fxadmin.com:180/index.php/v1/common/upload';

export function post_(url, data, callback, err){

    data.auth_key = localStorage.getItem('key')?localStorage.getItem('key'):''
    var qs = require("querystring")
    data = qs.stringify(data);
    axios.post(postUrl+url,data)
        .then((response)=>{
            if(response.data.code == 404){
                localStorage.clear();
                location.replace("#/Login")
                return
            }
            callback(response.data)
        })
        .catch(function (error) {
            console.log(error);
            err(error);
        });
}

export function ajax_upload(url, params, act)
{
    $.ajax({
        url:postUrl+url,
        data:params,
        type:'POST',
        dataType:'json',
        processData:false,
        contentType : false,
        // async:false,
        success:function(data){
            //$("#alert-bg").fadeOut(2000);
            act(data);
        },
        error:function(e){
            console.log('ajax error:',e);
            act(e.responseText);
        }
    });
}

export function download(url) {

    var iframe =document.createElement("iframe")

    iframe.style.display ="none";

    iframe.src = url;

    document.body.appendChild(iframe);
}
